# React + Node Survey App

* Survey app that lets one send out emails from which recipients can send reponses that are tracked by the survey creator

## Dependencies:
* [SendGrid](https://sendgrid.com/)
* [Swipe](https://stripe.com/ca)
* Oauth with Google
* [Mlabs](https://mlab.com/)

## Tech Stack:
* React
* Redux
* NodeJS
* ExpressJS
* MongoDB

## Links
* [App link](https://pure-plains-37560.herokuapp.com/)
* [Link to course](https://www.udemy.com/node-with-react-fullstack-web-development/)

## Notes
* In order to get the web hooks working in Dev mode need to add script to .json file: "webhook": "lt -p 5000 -s asldkjfaldskjf12skj12j31"


## NPM installs
* `npm install --save express`
* `npm install --save passport passport-google-oauth20`
* `npm install --save nodemon`
* `npm install --save mongoose`
* `npm install --save cookie-session`
* `npm install -g create-react-app`
* `npm install --save concurrently`
* `npm install http-proxy-middleware --save`
* `npm install --save redux react-redux react-router-dom`
* `npm install --save materialize-css`
* `npm install --save axios`
* `npm install --save redux-thunk`
* `npm install --save react-stripe-checkout`
* `npm install --save stripe`
* `npm install --save body-parser`
* `npm install --save sendgrid`
* `npm install --save redux-form`
* `npm install --save localtunnel`
* `npm install --save lodash path-parser`
