const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const User = mongoose.model('users');

//creates a cookie, using the user id of the document in MongoDb
passport.serializeUser((user,done) => {
  done(null, user.id);
});

passport.deserializeUser((id,done) => {
  User.findById(id)
    .then(user => {
      done(null, user);
    });
});

passport.use(new GoogleStrategy(
  {
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: keys.callbackURL
  },
  async (accessToken, refreshToken, profile, done) => {
    const existingUser = await User.findOne({ googleId: profile.id });
      
    if (existingUser) {
      done(null, existingUser);
    } else {
      const user = await new User({googleId: profile.id }).save();
      done(null, user);
    }
  }
));



// Some sample code showing different syntax for writing promises

// function fetchAlbums() {
//   fetch('https://rallycoding.herokuapp.com/api/music_albums')
//     .then(res => res.json())
//     .then(json => console.log(json));
// }

// is equivalent to:

//Note: put async in front of the outer function that contains any promise logic

// async function fetchAlbums() {
//   const res = await fetch('https://rallycoding.herokuapp.com/api/music_albums')
//   const json = await res.json()

//   console.log(json);
// }

// fetchAlbums();

