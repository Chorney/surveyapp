// SurveyNew shows SurveyForm and SurveyReviewForm
import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import SurveyForm from './SurveyForm';
import SurveyReviewForm from './SurveyFormReview';

class SurveyNew extends Component {
  state = { showFormReview: false}; //this is a shortcut over writing up an constructor. 100% equivalent


  renderContent() {
    if (this.state.showFormReview) {
      return <SurveyReviewForm onCancel={() => this.setState({showFormReview: false})}/>;
    }

    return <SurveyForm onSurveySubmit={() => this.setState({showFormReview: true})} />;
  }

  render() {
    return (
      <div>
        {this.renderContent()}
      </div>
    );
  }
}

export default reduxForm({
  form: 'surveyForm'
})(SurveyNew);