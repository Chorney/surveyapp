const keys = require('../config/keys');
const stripe = require('stripe')(keys.stripeSecretKey);
const requireLogin = require('../middlewares/requireLogin');

// middleware goes as the second option to the post, the posts, gets, etc etc, take an aribtrary amount of middleware functions
module.exports = (app) => {
  app.post('/api/stripe', requireLogin, async (req,res) => {
    const charge = await stripe.charges.create({
      amount: 500,
      currency: 'usd',
      description: '$5 for 5 credits',
      source: req.body.id
    });

    req.user.credits += 5; // this is setup auotmatically by password
    const user = await req.user.save();

    res.send(user);
  });
};